# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :sso,
  ecto_repos: [Hello.Repo],
  debug: false

config :sso, Sso.Guardian,
  issuer: "sso",
  secret_key: "Yw6+vDEDTgYUEvUDQ7WxGUVDd4EnIUgpzdhkvKRIFzMNhs+AUGhwefJ+koE+jjF0"

# General application configuration
config :hello,
  ecto_repos: [Hello.Repo]

# Configures the endpoint
config :hello, HelloWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "s7dh60KmFjseeao/p7Df7ac4qVP0bpxDeQOQRv1721w5Hx/G3iksksCKc+pUVYBy",
  render_errors: [view: HelloWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Hello.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
