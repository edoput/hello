defmodule HelloWeb.Router do
  use HelloWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :authenticated do
    plug Sso.AuthPipeline
  end

  pipeline :verified do
    plug Sso.IdentityPipeline
  end


  scope "/", HelloWeb do
    pipe_through :browser # Use the default browser stack
    pipe_through :verified
    pipe_through :authenticated

    get "/", PageController, :index
  end

  scope "/", SsoWeb do
    pipe_through :browser
    pipe_through :verified
    forward "/", Router
  end

  # Other scopes may use custom stacks.
  # scope "/api", HelloWeb do
  #   pipe_through :api
  # end
end
